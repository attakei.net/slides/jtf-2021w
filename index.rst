================================
プレゼン？それもSphinxで出来るよ
================================

-ドキュメントツールSphinxのちょっと変わった利用法-

:Date: 2021/1/24
:Author: `Kazuya Takei <https://attakei.net>`_
:Event: `July Tech Festa 2021 winter <https://techfesta.connpass.com/event/193966/>`_
:Hashtag: `#JTF2021w_d <https://twitter.com/hashtag/JTF2021w_d>`_

.. include:: _sections/introduction.rst

.. include:: _sections/sphinx-basic.rst

-> NEXT ->
==========

.. ここまでで、7分ぐらいが目安

.. include:: _sections/sphinx-presentation.rst

-> NEXT ->
==========

.. ここまでで、13分ぐらいが目安

ここからのメインは ``sphinx-revealjs``

.. include:: _sections/sphinx-revealjs.rst

.. ここまでで、20分ぐらいが目安

.. include:: _sections/conclusion.rst
